#!/usr/bin/python
import os
import sys
import re
import random
import pexpect

def tester(stateSize, inputSize, weights, experimentSize):
	with open("testresults_weighted_largest4_synchro.csv","w") as testresults:
		testresults.write("sep=,\n")
		#testresults.write("States,Inputs,Weights,Seed,Greedy1 Length,Greedy1 Weight,G1 Tree Time,G1 Algo Time,G1 Total Time,G1 No Actives,G1 Step,G1 Subsequences,G1 Subweights,Greedy2 Length,Greedy2 Weight,G2 Tree Time,G2 Algo Time,G2 Total Time,G2 No Actives,G2 Step,G2 Subsequences,G2 Subweights,Greedy3 Length,Greedy3 Weight,G3 Tree Time,G3 Algo Time,G3 Total Time,G3 No Actives,G3 Step,G3 Subsequences,G3 Subweights\n")
		#testresults.write("States,Inputs,Weights,Seed,SP Length,SP Weight,SP Tree Time,SP Algo Time,SP Total Time,SP No Actives,SP Step,SP Subsequences,SP Subweights,SPL Length,SPL Weight,SPL Tree Time,SPL Algo Time,SPL Total Time,SPL No Actives,SPL Step,SPL Subsequences,SPL Subweights,WSPL Length,WSPL Weight,WSPL Tree Time,WSPL Algo Time,WSPL Total Time,WSPL No Actives,WSPL Step,WSPL Subsequences,WSPL Subweights\n")
		testresults.write("States,Inputs,Weights,Seed,SqrtPL Length,SqrtPL Weight,SqrtPL Tree Time,SqrtPL Algo Time,SqrtPL Total Time,SqrtPL No Actives,SqrtPL Step,SqrtPL Subsequences,SqrtPL Subweights,BlncdPL Length,BlncdPL Weight,BlncdPL Tree Time,BlncdPL Algo Time,BlncdPL Total Time,BlncdPL No Actives,BlncdPL Step,BlncdPL Subsequences,BlncdPL Subweights,TBPL Length,TBPL Weight,TBPL Tree Time,TBPL Algo Time,TBPL Total Time,TBPL No Actives,TBPL Step,TBPL Subsequences,TBPL Subweights\n")
		for s in stateSize:
			for p in inputSize:
				for w in weights:
					expID = 1
					while expID < experimentSize:
						child = pexpect.spawn("./heuristics " + str(s) + " " + str(p) + " " + str(expID*2) + " arg " + str(w))
						child.expect([pexpect.EOF, pexpect.TIMEOUT], timeout=30000)
						if "synchronizing" in child.before.splitlines()[1]: 
							experimentSize += 1
						else:
							filename = "synchrop3_" + str(s) + "_" + str(p) + "_" + str(expID*2) + "_" + str(w) + ".txt"
							file = open(filename)
							content = file.readlines()
							for i in range(len(content)):
								content[i] = content[i].strip()
							testresults.write(str(s) + "," + str(p) + "," + str(w) + "," + str(expID*2)  + ",")
							testresults.write(content[8][content[8].rfind("is")+3:] + ",")
							testresults.write(content[9][content[9].rfind("is")+3:] + ",")
							testresults.write(content[0][content[0].rfind("takes")+6:content[0].rfind("seconds")-1] + ",")
							testresults.write(content[5][content[5].rfind("takes")+6:content[5].rfind("seconds")-1] + ",")
							testresults.write(content[6][content[6].rfind("takes")+6:content[6].rfind("seconds")-1] + ",")
							testresults.write(content[1] + ",")
							testresults.write(content[2] + ",")
							testresults.write(content[3] + ",")
							testresults.write(content[4] + ",")
							testresults.write(content[19][content[19].rfind("is")+3:] + ",")
							testresults.write(content[20][content[20].rfind("is")+3:] + ",")
							testresults.write(content[11][content[11].rfind("takes")+6:content[11].rfind("seconds")-1] + ",")
							testresults.write(content[16][content[16].rfind("takes")+6:content[16].rfind("seconds")-1] + ",")
							testresults.write(content[17][content[17].rfind("takes")+6:content[17].rfind("seconds")-1] + ",")
							testresults.write(content[12] + ",")
							testresults.write(content[13] + ",")
							testresults.write(content[14] + ",")
							testresults.write(content[15] + ",")
							testresults.write(content[30][content[30].rfind("is")+3:] + ",")
							testresults.write(content[31][content[31].rfind("is")+3:] + ",")
							testresults.write(content[22][content[22].rfind("takes")+6:content[22].rfind("seconds")-1] + ",")
							testresults.write(content[27][content[27].rfind("takes")+6:content[27].rfind("seconds")-1] + ",")
							testresults.write(content[28][content[28].rfind("takes")+6:content[28].rfind("seconds")-1] + ",")
							testresults.write(content[23] + ",")
							testresults.write(content[24] + ",")
							testresults.write(content[25] + ",")
							testresults.write(content[26] + ",")
							testresults.write("\n")
							expID += 1	
def main():
	"""
	stateSize = [32, 64]
	inputSize = [2, 4]
	weights = [2, 4]
	experimentSize = 2
	"""
	stateSize = [256, 512, 1024]
	inputSize = [2, 4, 8]
	weights = [1, 4, 16, 64] #weights = [1, 2, 4, 8, 16, 32, 64]
	experimentSize = 51
	
	tester(stateSize, inputSize, weights, experimentSize)
	
if __name__ == "__main__":
	main()
