import pexpect

def tester(stateSize, inputSize, weights, experimentSize):
	tempexpSize = experimentSize
	sumReg = 0
	sumNew = 0
	for s in stateSize:
		for p in inputSize:
			for w in weights:
				experimentSize = tempexpSize
				for expID in range(1, experimentSize):
					#print "states: ", s, "inputs: ", p, "Weight: ", w, "seed: ", expID*2
	
					child = pexpect.spawn("./greedy " + str(s) + " " + str(p) + " 1 " + str(expID*2) + " rand " + str(w))
					child.expect(pexpect.EOF)
					print child.before
					sumReg += int(child.before.splitlines()[0][child.before.splitlines()[0].find('is') + 3:].strip())
					sumNew += int(child.before.splitlines()[1][child.before.splitlines()[1].find('is') + 3:].strip())

				print "states: ", s, "inputs: ", p, "Weight: ", w, "AVG_G: ", sumReg/experimentSize, 'AVG_NEW: ', sumNew/experimentSize
				sumReg = 0
				sumNew = 0

def main():
    stateSize = [256, 512, 1024]
    inputSize = [2, 4, 8]
    weights = [1, 2, 8, 16, 32, 64]
    experimentSize = 5
    tester(stateSize, inputSize, weights, experimentSize)
       
if __name__ == "__main__":
    main()