\documentclass[11pt]{article} 
\usepackage{rotating}
\usepackage{epsfig} 
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{color}
\usepackage{fullpage}
\usepackage{varwidth}

\newcommand{\answer}{answer}
\newcommand{\hycomment}[1]{{\color{blue}{#1}}}
\newcommand{\nescomment}[1]{{\color{green}{#1}}}
\newcommand{\bccomment}[1]{{\color{magenta}{#1}}}
\newcommand{\kkcomment}[1]{{\color{red}{#1}}}

\newcommand{\Ab}{\mathbf{A}}
\newcommand{\xb}{\mathbf{x}}
\newcommand{\wb}{\mathbf{w}}
\newcommand{\vb}{\mathbf{v}}

\title{Revision Report for ``Synchronizing Heuristics for Weighted Automata''}

\author{N. Ege Sara\c{c}, Berk \c{C}iri\c{s}ci, Kamer Kaya, H\"{u}sn\"{u} Yenig\"{u}n}

\begin{document} 
\maketitle
\vspace*{-3ex} 

Thanks to the reviewers and editor-in-chief for the comments. The changes in the manuscript are outlined below based on the concerns raised in the comments. 
Other minor changes are also performed in the manuscript for better English usage.\\

\section*{Reviewers' comments:}
Thank you for showing appreciation of the content of the paper. Below, the comments are repeated in bold face and followed by our responses. 
\vspace*{2ex}

\subsection*{Reviewer 1:} 
{\bf The paper considers the problem of finding ``short (or cheap)" synchronizing
words for (deterministic) finite automata. 
While the traditional notion of ``short" is just the length of the 
synchronizing word, the paper considers weighted automata (each transition 
carries a weight) and the cost of a word is the sum of the weights along
the run of the weighted automaton on the word.}
\vspace*{2ex}

{\bf The paper is mostly of experimental nature, and proposes some heuristics
to compute ``cheap" synchronizing words. A standard heuristics for finite automata
is to successively synchronize pairs of states. This standard heuristics 
is adapted to weighted automata, where the freedom is in the choice of
which two states to synchronize among the remaining active states,
in order to minimize the total cost.}
\vspace*{2ex}

{\bf The paper presents various heuristics for this choice. The heuristics are 
simple and natural. Experiments have been done on randomly generated 
automata, and the paper compares the cost of the synchronizing words
found by the different heuristics.}
\vspace*{1ex}

We agree with the summary. Thanks for the nice comments. 
\vspace*{2ex}

{\bf The contribution of the paper is very thin, and mostly at the level of an 
undergraduate student project. Perhaps identifying that the problem had not
been addressed before can be put at the credit of the paper, but there is no 
other significant new idea in the paper and the experimental evaluation does 
not really provide relevant new knowledge.}
\vspace*{2ex}

We agree with the comment of our reviewer in that, this is a novel problem. And, 
we would like to add that it is novel problem without a trivial solution, as we
point out in the paper. Hence, we cannot agree with the comment that the contribution is thin.

When we started working on this topic, we initially thought that it would be 
sufficient to modify Phase 1 of the existing heuristics (i.e. use Dijkstra's shortest path algorithm instead of BFS on the pair automaton formed with 
weighted edges).
This worked out as expected for the input--weighted model, however things were
not as intuitive for the transition weighted model. The solution suggested 
in the paper for the transition weighted model is backed up with a serious
research work taking time in the order of several months. We had to analyze
the experimental results to get an insight why direct application of the 
heuristics do not work, come up with modifications of these heuristics 
accordingly, and perform several iterations of these improvement steps. 


\vspace*{2ex}


\subsection*{Reviewer 2:} 
{\bf The paper discusses the problem of finding short synchronization sequences in automata where transitions can have different weights. It is reasonably well written, explores the obvious possibilities and reports on experimental evaluation thereof. In my opinion it is ok to accept the paper after the issues below are clarified.}
\vspace*{2ex}

Thanks for the nice comments. 
\vspace*{2ex}

{\bf Most importantly, since the paper introduces this topic, the definitions should be better justified. In particular, the weight W(C,w) for a set C and word w is taken as the sum of maxima for each letter and not the maximum of sums for each run. Is this to model time flow or similar phenomena? Since both definitions make similar sense, the choice should be discussed.}
\vspace*{2ex}

Thanks for pointing this out. In fact, during the early stages of this study, we also had extensive discussions on the correct cost model and come up with several ideas. 
To include a brief summary of our discussions, introduce new cost models, and clarify what each weighted automata- and cost-model implies, we added the following to Section~2 and Section~3, respectively. 

\begin{center}
\fbox{\begin{varwidth}{\dimexpr\textwidth-2\fboxsep-2\fboxrule\relax}
In this work, we will focus on two weighted automata models: 
\begin{enumerate}
\item {\em Input-weighted}: If for all $x \in X$ and $s,s' \in S$, $W(s, x) = W(s', x)$ then $A$ is input-weighted, i.e., the cost incurred by each transition solely depends on the input but not the state. 
\item {\em Transition-weighted}: If there exists $x \in X$ and $s,s' \in S$ such that $W(s, x) \neq W(s', x)$ then $A$ is input-weighted, i.e., a transition cost depends on both the state and the input.
\end{enumerate}
\end{varwidth}}
\end{center}

\begin{center}
\fbox{\begin{varwidth}{\dimexpr\textwidth-2\fboxsep-2\fboxrule\relax}
To define the synchronization cost on weighted automata, one needs to extend the weight function $W$ to a state set and a sequence. Possible extension alternatives, each yielding a different cost model, are given below;
\begin{eqnarray}
W(C,x w) =& W(C,x) + W (\delta(C,x), w) \textnormal{ s.t. } W(C, x) = \max\limits_{s\in C} \{W(s, x)\}\label{cf1}\\
W(C,x w) =& \max\limits_{s\in C} \{W(s, xw)\} \textnormal{ s.t. } W(s, xw) = W(s, x) +  W(\delta(s,x), w)\label{cf2}\\
W(C,x w) =&  \underset{s\in C}{\textnormal{avg}} \{W(s, xw)\} \textnormal{ s.t. } W(s, xw) = W(s, x) +  W(\delta(s,x), w)\label{cf3}
\end{eqnarray}
For all extensions, the cost for a synchronizing sequence $w$ is $W(w) = W(S, w)$ where $S$ is the set of all states. Note that, for an unweighted automaton and all extensions, $W(w) = |w|$.
The synchronizing heuristics used in this work try to find a sequence $w$ with a small $W(w)$ value. 
Note that~\eqref{cf1} is required when it is not possible to know if the device is ready for the next input/transition. In this case, the weights correspond the transition time. To guarantee that the previous transition is completed, one needs to wait for the maximum possible duration. 
The next two equations are applicable when it is possible to move to the next transition immediately, once the previous one is completed. 
For this case,~\eqref{cf2} denotes an upper bound and~\eqref{cf3} denotes the expected cost.
\\

For an input-weighted automaton, $W(C, w) = W(C', w)$ for any $C, C' \subseteq S$. 
Therefore, there is no difference between~\eqref{cf1},~\eqref{cf2} and~\eqref{cf3} for the input-weighted model. 
However, for a transition-weighted automaton, each equation corresponds to a different use case. In this work, 
we will focus on~\eqref{cf1}, which is arguably more generic, and leave the other two as a future work.

\end{varwidth}}
\end{center}

{\bf  Related work: although there may be no work on short synchronization with weights on edges, some related work on synchronization in quantitative settings should be cited, e.g. the line of work by Doyen\&Massart\&Shirmohammadi for MDP or Kretinsky\&Larsen\&Laursen\&Srba for weighted systems.}
\vspace*{2ex}

Thanks for pointing out these studies from the literature; we cited them in the manuscript. 
\vspace*{2ex}

{\bf Although the heuristic CYCLE is named together with GREEDY and SYNCHRO, further discussion is limited to the latter without any comments.}
\vspace*{2ex}

To keep the discussion to the point and to avoid confusion, we removed the CYCLE reference from the Introduction but kept it in the Conclusion and Future Work section. 
\vspace*{2ex}

{\bf The heuristic for tie breaking is clear in the parts P1 and P2, but what is P3 and why is it used?}
\vspace*{2ex}

Thanks for the remark; to be more clear about our intuition, we added the following sentence after defining the breaking mechanism:

\begin{center}
\fbox{\begin{varwidth}{\dimexpr\textwidth-2\fboxsep-2\fboxrule\relax}
The intuition is that we want a merging sequence that is less costly~($P_1$), yields to a smaller active set~($P_2$) which is {\em potentially} also less costly to synchronize~($P_3$).
\end{varwidth}}
\end{center}
\vspace*{2ex}

{\bf The experimental evaluation is done on 50 random automata. Why not several hundred when they are anyway randomly generated? }
\vspace*{2ex}

To understand the representativeness of the samples, we measured the dispersions on the sampled values; 
for each algorithm, and for given number of states~(in $\{256, 512, 1024\}$), number of inputs~(in $\{2,4,8\}$), 
and variation on weights~(in~$\{1,4,16,64\}$), we computed the Coefficient of Variation~(CV), i.e., the ratio of 
the sample standard deviation to the sample mean.

For Greedy and its variants ActiveGreedy and TiebreakGreedy, the average of the CV values are only $7\%$.
Furthermore, the standard deviation of these CV values for each algorithm are $2\%, 2\%$, and $3\%$, respectively. 
Similarly, for SynchroP and its weighted variants, the averages of the CV values are in between $6\%$ and $7\%$.
Furthermore, the maximums of the CVs are in between $10\%$ and $11\%$, and the standard deviations are all equal to $2\%$. 
Hence, for all algorithms and possible parameter sets, whose average is reported in the paper, the dispersion among the random trials are very low.  

We also checked the confidence intervals for the results of our experiments. This approach similarly showed that the dispersion is quite low. 
To give an example, for the largest automata size we used in our experiments (1024 states, 8 inputs, and with 64 weight variation) for the confidence level of 95\%, we obtained the confidence intervals of $[4120, 4225]$, $[4059,4154]$, $[4021, 4110]$, $[4029,4120]$, $[4025,4103]$, and $[3951,4031]$ for the weight of the synchronizing sequence found by SynchroP, SynchroPL, ActiveSynchroPL, SqrtPL, TiebreakPL, and HybridPL, respectively. We believe these observations show that the results are representative.

We also note that, overall, running only the final versions of the proposed heuristics took around a week on our machine.
Considering that we spent much more than that by running their initial forms on the full set of automata, and eliminate/modify the inferior ones, 
we spent around a month for all the experiments for this study. 

\vspace*{2ex}

{\bf ``The reason for this speed up is the reduction in the number of iterations" - please expand this; this is not clear at all.}
\vspace*{2ex}

To make this observation more clear, we change the statement as follows by adding further explanation:

\begin{center}
\fbox{\begin{varwidth}{\dimexpr\textwidth-2\fboxsep-2\fboxrule\relax}
Timewise, \textsc{InputWeightedGreedy} runs 2.39 times slower than \textsc{Greedy} on the average. However, \textsc{InputWeightedSynchroP} and \textsc{InputWeightedSynchroPL} are 15-16\% faster compared to the \textsc{SynchroP} and \textsc{SynchroPL}, respectively. The reason for this speed-up is the reduction in the number of iterations of Phase~2 in \textsc{InputWeightedSynchroP} and \textsc{InputWeightedSynchroPL}.
That is at a single step, the original heuristics tend to use shorter sequences and hence yield larger active sets and more total number of iterations. On the other hand, the cost-aware heuristics do not explicitly care about the sequence length and as a side effect, can employ less number of iterations. However, they almost always yield longer sequences.
\end{varwidth}}
\end{center}
\vspace*{2ex}

{\bf Fig.1: the legend is tiny, axes not clear}
{\bf Fig.2: SqrtPL and TPL cannot be seen in the graph}
\vspace*{2ex}

Thanks for pointing these out - to improve the visualization, we now use colored markers. Furthermore, the legends of the figures are relocated and they are resized to be slightly larger. 
\vspace*{2ex}

{\bf HYBRIDPL vs. SYNCHOPL - what about the remaining 4. It can't be worse, can it?}
\vspace*{2ex}

The HybridPL heuristic can be considered as the weighted version developed based on the ideas in SynchroPL; however, it does not contain SynchroPL. Since the uniformity in the weights make the problem harder to approach, there are cases, $4\%$ of the experiments, in which HybridPL performs slightly worse than SynchroPL.

\vspace*{2ex}

\subsection*{Reviewer 3:} 
{\bf I think that this paper discusses a fairly novel problem but I struggled to understand to what extent this problem is really of significance. The definitions of the problem and of the heuristics should be improved by adding a clearer and stronger justification of the relevance of the problem and of the proposed solution. In particular, the heuristics look quite standard and obvious and I was left wondering if more refined ones could be given and whether they could be generalized to other forms of automata.}
\vspace*{2ex}

{\bf I recommend that the authors expand on this in more detail.}
\vspace*{2ex}

Thanks for the nice comments. We agree that the problem is novel and is really of significance. To make our contributions more clear, we revised the text and structure and added a possible application paragraph that matches the proposed problem with real life as follows:

\begin{center}
\fbox{\begin{varwidth}{\dimexpr\textwidth-2\fboxsep-2\fboxrule\relax}
A synchronizing sequence is a fool-proof remedy to regain the control of a system, or reset it, when its current state is unreachable~(i.e., assume all the communication channels, sensors, etc., are disabled or even, hacked). Consider mobile robots working in a factory; a synchronizing sequence, that can be embedded to a tamper-proof local storage, can yield each robot to a functional, secure state or direct it to a specific point such as a repair station. Similar problems can be stated for  autonomous cars in the traffic or drones collectively working on a field. Although, such problems can be modeled with an equi-weighted automata, the transition costs are important if one cares about the time and energy spent for synchronization. For the factory example, the time spent to bring the machine to the functional state is important to increase the utilization. For more mobile systems such as drones, energy consumption can be more of a concern. Note that customized weighting schemes with  extra parameters are also possible. Furthermore, even after a weighted automaton is constructed, there can be multiple synchronization cost models; for one application, it may be better to reduce the expected synchronization cost whereas for another, the worst case, i.e., an upper-bound on the cost, may be more important.\looseness=-1
\end{varwidth}}
\end{center}

\end{document}








