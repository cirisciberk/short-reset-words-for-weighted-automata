#!/usr/bin/python
import os
import sys
import re
import random
import pexpect

def tester(stateSize, inputSize, weights, experimentSize):
	with open("testresults_weighted_largest3.csv","w") as testresults:
		testresults.write("sep=,\n")
		testresults.write("States,Inputs,Weights1,Seed,SP1 Length,SP1 Weight,SP1 Tree Time,SP1 Algo Time,SP1 Total Time,SP1 No Actives,SP1 Step,SP1 Subsequences,SP1 Subweights,WSP Length,WSP Weight,WSP Tree Time,WSP Algo Time,WSP Total Time,WSP No Actives,WSP Step,WSP Subsequences,WSP Subweights,Greedy1 Length,Greedy1 Weight,G1 Tree Time,G1 Algo Time,G1 Total Time,G1 No Actives,G1 Step,G1 Subsequences,G1 Subweights,WG Length,WG Weight,WG Tree Time,WG Algo Time,WG Total Time,WG No Actives,WG Step,WG Subsequences,WG Subweights,Weights2,SP2 Length,SP2 Weight,SP2 Tree Time,SP2 Algo Time,SP2 Total Time,SP2 No Actives,SP2 Step,SP2 Subsequences,SP2 Subweights,P-WBESP Length,P-WBESP Weight,P-WBESP Tree Time,P-WBESP Algo Time,P-WBESP Total Time,P-WBESP No Actives,P-WBESP Step,P-WBESP Subsequences,P-WBESP Subweights,WBESP Length,WBESP Weight,WBESP Tree Time,WBESP Algo Time,WBESP Total Time,WBESP No Actives,WBESP Step,WBESP Subsequences,WBESP Subweights,Greedy2 Length,Greedy2 Weight,G2 Tree Time,G2 Algo Time,G2 Total Time,G2 No Actives,G2 Step,G2 Subsequences,G2 Subweights,P-WBGreedy Length,P-WBGreedy Weight,P-WBG Tree Time,P-WBG Algo Time,P-WBG Total Time,P-WBG No Actives,P-WBG Step,P-WBG Subsequences,P-WBG Subweights,WBEG Length,WBEG Weight,WBEG Tree Time,WBEG Algo Time,WBEG Total Time,WBEG No Actives,WBEG Step,WBEG Subsequences,WBEG Subweights\n")
		for s in stateSize:
			for p in inputSize:
				for w in weights:
					expID = 1
					while expID < experimentSize:
						filename = "synchrop1_" + str(s) + "_" + str(p) + "_" + str(expID*2) + "_" + str(w) + ".txt"
						file = open(filename)
						content = file.readlines()
						for i in range(len(content)):
							content[i] = content[i].strip()
						testresults.write(str(s) + "," + str(p) + "," + content[23] + "," + str(expID*2)  + ",")
						testresults.write(content[8][content[8].rfind("is")+3:] + ",")
						testresults.write(content[9][content[9].rfind("is")+3:] + ",")
						testresults.write(content[0][content[0].rfind("takes")+6:content[0].rfind("seconds")-1] + ",")
						testresults.write(content[5][content[5].rfind("takes")+6:content[5].rfind("seconds")-1] + ",")
						testresults.write(content[6][content[6].rfind("takes")+6:content[6].rfind("seconds")-1] + ",")
						testresults.write(content[1] + ",")
						testresults.write(content[2] + ",")
						testresults.write(content[3] + ",")
						testresults.write(content[4] + ",")
						testresults.write(content[19][content[19].rfind("is")+3:] + ",")
						testresults.write(content[20][content[20].rfind("is")+3:] + ",")
						testresults.write(content[11][content[11].rfind("takes")+6:content[11].rfind("seconds")-1] + ",")
						testresults.write(content[16][content[16].rfind("takes")+6:content[16].rfind("seconds")-1] + ",")
						testresults.write(content[17][content[17].rfind("takes")+6:content[17].rfind("seconds")-1] + ",")
						testresults.write(content[12] + ",")
						testresults.write(content[13] + ",")
						testresults.write(content[14] + ",")
						testresults.write(content[15] + ",")
						
						filename = "greedy1_" + str(s) + "_" + str(p) + "_" + str(expID*2) + "_" + str(w) + ".txt"
						file = open(filename)
						content = file.readlines()
						for i in range(len(content)):
							content[i] = content[i].strip()
						testresults.write(content[8][content[8].rfind("is")+3:] + ",")
						testresults.write(content[9][content[9].rfind("is")+3:] + ",")
						testresults.write(content[0][content[0].rfind("takes")+6:content[0].rfind("seconds")-1] + ",")
						testresults.write(content[5][content[5].rfind("takes")+6:content[5].rfind("seconds")-1] + ",")
						testresults.write(content[6][content[6].rfind("takes")+6:content[6].rfind("seconds")-1] + ",")
						testresults.write(content[1] + ",")
						testresults.write(content[2] + ",")
						testresults.write(content[3] + ",")
						testresults.write(content[4] + ",")
						testresults.write(content[19][content[19].rfind("is")+3:] + ",")
						testresults.write(content[20][content[20].rfind("is")+3:] + ",")
						testresults.write(content[11][content[11].rfind("takes")+6:content[11].rfind("seconds")-1] + ",")
						testresults.write(content[16][content[16].rfind("takes")+6:content[16].rfind("seconds")-1] + ",")
						testresults.write(content[17][content[17].rfind("takes")+6:content[17].rfind("seconds")-1] + ",")
						testresults.write(content[12] + ",")
						testresults.write(content[13] + ",")
						testresults.write(content[14] + ",")
						testresults.write(content[15] + ",")
						
						filename = "synchrop2_" + str(s) + "_" + str(p) + "_" + str(expID*2) + "_" + str(w) + ".txt"
						file = open(filename)
						content = file.readlines()
						for i in range(len(content)):
							content[i] = content[i].strip()
						testresults.write(content[35] + ",")
						testresults.write(content[8][content[8].rfind("is")+3:] + ",")
						testresults.write(content[9][content[9].rfind("is")+3:] + ",")
						testresults.write(content[0][content[0].rfind("takes")+6:content[0].rfind("seconds")-1] + ",")
						testresults.write(content[5][content[5].rfind("takes")+6:content[5].rfind("seconds")-1] + ",")
						testresults.write(content[6][content[6].rfind("takes")+6:content[6].rfind("seconds")-1] + ",")
						testresults.write(content[1] + ",")
						testresults.write(content[2] + ",")
						testresults.write(content[3] + ",")
						testresults.write(content[4] + ",")
						testresults.write(content[19][content[19].rfind("is")+3:] + ",")
						testresults.write(content[20][content[20].rfind("is")+3:] + ",")
						testresults.write(content[11][content[11].rfind("takes")+6:content[11].rfind("seconds")-1] + ",")
						testresults.write(content[16][content[16].rfind("takes")+6:content[16].rfind("seconds")-1] + ",")
						testresults.write(content[17][content[17].rfind("takes")+6:content[17].rfind("seconds")-1] + ",")
						testresults.write(content[12] + ",")
						testresults.write(content[13] + ",")
						testresults.write(content[14] + ",")
						testresults.write(content[15] + ",")
						testresults.write(content[30][content[30].rfind("is")+3:] + ",")
						testresults.write(content[31][content[31].rfind("is")+3:] + ",")
						testresults.write(content[22][content[22].rfind("takes")+6:content[22].rfind("seconds")-1] + ",")
						testresults.write(content[27][content[27].rfind("takes")+6:content[27].rfind("seconds")-1] + ",")
						testresults.write(content[28][content[28].rfind("takes")+6:content[28].rfind("seconds")-1] + ",")
						testresults.write(content[23] + ",")
						testresults.write(content[24] + ",")
						testresults.write(content[25] + ",")
						testresults.write(content[26] + ",")
						
						filename = "greedy2_" + str(s) + "_" + str(p) + "_" + str(expID*2) + "_" + str(w) + ".txt"
						file = open(filename)
						content = file.readlines()
						for i in range(len(content)):
							content[i] = content[i].strip()
						testresults.write(content[8][content[8].rfind("is")+3:] + ",")
						testresults.write(content[9][content[9].rfind("is")+3:] + ",")
						testresults.write(content[0][content[0].rfind("takes")+6:content[0].rfind("seconds")-1] + ",")
						testresults.write(content[5][content[5].rfind("takes")+6:content[5].rfind("seconds")-1] + ",")
						testresults.write(content[6][content[6].rfind("takes")+6:content[6].rfind("seconds")-1] + ",")
						testresults.write(content[1] + ",")
						testresults.write(content[2] + ",")
						testresults.write(content[3] + ",")
						testresults.write(content[4] + ",")
						testresults.write(content[19][content[19].rfind("is")+3:] + ",")
						testresults.write(content[20][content[20].rfind("is")+3:] + ",")
						testresults.write(content[11][content[11].rfind("takes")+6:content[11].rfind("seconds")-1] + ",")
						testresults.write(content[16][content[16].rfind("takes")+6:content[16].rfind("seconds")-1] + ",")
						testresults.write(content[17][content[17].rfind("takes")+6:content[17].rfind("seconds")-1] + ",")
						testresults.write(content[12] + ",")
						testresults.write(content[13] + ",")
						testresults.write(content[14] + ",")
						testresults.write(content[15] + ",")
						testresults.write(content[30][content[30].rfind("is")+3:] + ",")
						testresults.write(content[31][content[31].rfind("is")+3:] + ",")
						testresults.write(content[22][content[22].rfind("takes")+6:content[22].rfind("seconds")-1] + ",")
						testresults.write(content[27][content[27].rfind("takes")+6:content[27].rfind("seconds")-1] + ",")
						testresults.write(content[28][content[28].rfind("takes")+6:content[28].rfind("seconds")-1] + ",")
						testresults.write(content[23] + ",")
						testresults.write(content[24] + ",")
						testresults.write(content[25] + ",")
						testresults.write(content[26] + ",")
						testresults.write("\n")
					expID += 1	
def main():
	"""
	stateSize = [256, 512]
	inputSize = [2, 4, 8]
	weights = [2,16, 64]
	experimentSize = 2
	"""
	stateSize = [256, 512, 1024]
	inputSize = [2, 4, 8]
	weights = [1, 2, 4, 8, 16, 32, 64]
	experimentSize = 51
	
	
	tester(stateSize, inputSize, weights, experimentSize)
	
if __name__ == "__main__":
	main()
