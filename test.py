#!/usr/bin/python
import os
import sys
import re
import random
import pexpect

def tester(stateSize, inputSize, weights, experimentSize):
    tempexpSize = experimentSize
    testresults = open("testresults_weighted.csv","w")
    testresults.write("sep=,\n")
    testresults.write("States,Inputs,Weight Dist,Seed,SP Length,SP Weight,SP Time,WSP Length,WSP Weight,WSP Time,Greedy Length,Greedy Weight,Greedy Time,WG Length,WG Weight,WG Time,PA Length,PA Weight\n")
    for s in stateSize:
      for p in inputSize:
        for w in weights:
            experimentSize = tempexpSize
            for expID in range(1, experimentSize):
                weight = ""
                for i in range(p):
                    weight += str(random.randint(1,w)) + " "
                print "states: ", s, "inputs: ", p, "Weight: ", weight, "seed: ", expID*2
                
                child = pexpect.spawn("./heuristics " + str(s) + " " + str(p) + " " + str(expID*2) + " arg " + weight)
                child.expect(pexpect.EOF)
                if "synchronizing" in child.before.splitlines()[1]: 
                    experimentSize += 1
                else:
					testresults.write(str(s) + "," + str(p) + "," + weight + "," + str(expID*2)  + ",")
					testresults.write(child.before.splitlines()[4][child.before.splitlines()[4].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[5][child.before.splitlines()[5].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[2][child.before.splitlines()[2].rfind("takes")+6:] + ",")
					testresults.write(child.before.splitlines()[12][child.before.splitlines()[12].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[13][child.before.splitlines()[13].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[9][child.before.splitlines()[9].rfind("takes")+6:] + ",")
					child = pexpect.spawn("./greedy " + str(s) + " " + str(p) + " 1 " + str(expID*2) + " arg " + weight)
					child.expect(pexpect.EOF)
					testresults.write(child.before.splitlines()[4][child.before.splitlines()[4].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[5][child.before.splitlines()[5].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[2][child.before.splitlines()[2].rfind("takes")+6:] + ",")
					testresults.write(child.before.splitlines()[11][child.before.splitlines()[11].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[12][child.before.splitlines()[12].rfind("is")+3:] + ",")
					testresults.write(child.before.splitlines()[8][child.before.splitlines()[8].rfind("takes")+6:] + ",")
					testresults.write(child.before.splitlines()[15][child.before.splitlines()[15].rfind(":")+1:] + ",")
					testresults.write(child.before.splitlines()[16][child.before.splitlines()[16].rfind(":")+1:])
					testresults.write("\n")
    
    testresults.close()    

def main():
    stateSize = [8, 16, 24]
    inputSize = [2, 4, 8]
    weights = [1, 2 ,8, 16, 32, 64]
    experimentSize = 1000
    tester(stateSize, inputSize, weights, experimentSize)
       
if __name__ == "__main__":
    main()
